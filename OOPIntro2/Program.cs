﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NameSpace1;
using NameSpace2;
using OOPIntro2;




namespace OOPIntro2
{
    class Program
    {
        /*Please set path of the path to use here*/
        public static string PATH = "C:/users/tjohn/desktop/cutomers.txt";

        static void Main(string[] args)
        {
            Welcome welcome = new Welcome();

            string name = welcome.getName();

            welcome.welcomeUser(name);
            welcome.explainCommands();

        }
    }

    class Welcome
    {
        public string getName()
        {
            Console.WriteLine("Enter your name:");
            string name = Console.ReadLine();
            return name;
        }

        public void welcomeUser(string name) {
            Console.WriteLine("Hi {0}, you're welcome to simple oop program", name);
        }

        public void explainCommands()
        {
            Console.WriteLine("Enter new customer information, read it and get total amount paid by cutomers");
            Console.WriteLine("Here we go:");
            Console.WriteLine("Enter 'a' to add new Customer");
            Console.WriteLine("Enter 'v' to view all Customers");
            Console.WriteLine("Enter 'v1' to view 1 Customers");
            Console.WriteLine("Enter 't' to see total amount paid so far");
            Console.WriteLine("Enter 'exit' to end the program");
            Commands commands = new Commands();
            commands.executeCommand();
        }


    }

    class FileManipulation
    {
        private static  void writeFile(string path, string[] lines)
        {

            File.WriteAllLines(path, lines);

        }

        public void insertToFile(string[] lines)
        {
            string path = Program.PATH;

            if (!File.Exists(path))
            {
                writeFile(path, lines);
            }

            File.AppendAllLines(path, lines);
        }

        public string[] readFile(string filename)
        {

            string path = Program.PATH;



            if (!File.Exists(path))
            {
                return null;
            }

            return File.ReadAllLines(path);
        }


    }
}
namespace NameSpace1
{
    class Customer
    {
        private string fname;
        private string lname;
        private string gender;
        private double amountPaid;

        public Customer(string fname, string lname, string gender, double amountPaid)
        {
            this.fname = fname;
            this.lname = lname;
            this.gender = gender;
            this.amountPaid = amountPaid;
        }

        public string getFname()
        {
            return this.fname;
        }


        public string getLname()
        {
            return this.lname;
        }

        public string getGender()
        {
            return this.gender;
        }

        public double getAmountPaid()
        {
            return this.amountPaid;
        }


    }

    static class Validator
    {
        static public bool validateName(string name)
        {
            if (Regex.Match(name, "^[a-zA-Z]{2,100}$").Success)
                return true;

            else
            {
                Console.WriteLine("Invalid name");
                return false;
            }
        }

        static public bool validateGender(string gender)
        {
            if (gender.ToLower().Equals("male") || gender.ToLower().Equals("female"))
                return true;

            else
            {
                Console.WriteLine("Invalid gender");
                return false;
            }
        }

        static public bool validateAmount(string amount)
        {
            double value;
            if (double.TryParse(amount, out value))
            { 
                return true;
            }
            else
            {
                Console.WriteLine("Invalid amount");
                return false;
            }
                
        }
    }

    static class ValidateCustomer
    {
        public static  bool validateAll(string[] customerInfo)
        {
            if(customerInfo.Length == 4)
            {
                if(Validator.validateName(customerInfo[0]) && Validator.validateName(customerInfo[1]) 
                    && Validator.validateGender(customerInfo[2]) && Validator.validateAmount(customerInfo[3]))
                {
                    return true;
                }
            }
            Console.WriteLine("Invalid input, please try again.");
            return false;
        }
    }
}

namespace NameSpace2
{
    class Commands
    {
       
        private static string getCommand()
        {
            Console.WriteLine("Enter a command:");
            return Console.ReadLine();
        }

        public static string validateCommand()
        {
           

            string[] commands = { "a", "v", "v1", "t", "exit"};
            string command = getCommand();
            bool wrongCommand = Array.IndexOf(commands, command) < 0;
            while (wrongCommand)
            {
                Console.WriteLine("You entered an invalid command");
                command = getCommand();
                wrongCommand = Array.IndexOf(commands, command) < 0;
            }

            return command;
        }

        public void executeCommand()
        {
            string command = validateCommand();
            Commands commands = new Commands();
            if (command == "a")
            {
                

                bool correctInput = false;
                string[] input = null;
                while (!correctInput)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Enter customer info separated by space in the following format:");
                    Console.WriteLine("FirstName LastName Gender AmountPaid");
                    input = Console.ReadLine().Split(' ');
                    if (ValidateCustomer.validateAll(input))
                    {
                        correctInput = true;
                    }
                }

                Customer customer = new Customer(input[0], input[1], input[2], double.Parse(input[3]));
                InputExecutor.insertCustomer(customer);
                Console.WriteLine("Customer data added..");
                Console.WriteLine("");
                commands.executeCommand();
            }
            else if(command == "v")
            {
                Console.WriteLine("Reading all cutomers..");
                OutputExecutors.displayAll();
                commands.executeCommand();
            }
            else if(command == "v1")
            {
                OutputExecutors.displayOne();
                commands.executeCommand();
            }
            else if(command == "t"){
                Console.WriteLine("Getting total amount paid by customers..");
                OutputExecutors.displayTotal();
                commands.executeCommand();
            }
            else
            {

            }
        }


    }

    class OutputExecutors
    {
        static FileManipulation f = new FileManipulation();
        public static void displayAll()
        {
            string[] customers = f.readFile("customers");
            if (customers == null)
            {
                Console.WriteLine("No record Found!!");
                return;
            }
            Console.WriteLine("");
            Console.WriteLine("**************************************");
            Console.WriteLine("List of inputed Customers");
            foreach (string customer in customers)
            {
                Console.WriteLine(customer);
            }
        }

        public static void displayTotal()
        {
            string[] customers = f.readFile("customers");
            if(customers == null)
            {
                Console.WriteLine("No record Found!!");
                return;
            }
            double total = 0;
            foreach (string customer in customers)
            {
                total += double.Parse(customer.Split('\t')[3]);
            }
            Console.WriteLine("");
            Console.WriteLine("**************************************");
            Console.WriteLine("Total Amount paid in is: {0}", total);

        }

        public static void  displayOne()
        {
            Console.WriteLine("Enter customer first name to search:");
            string fname = Console.ReadLine();
            string[] customers = f.readFile("customers");
            if (customers == null)
            {
                Console.WriteLine("No record Found!!");
                return;
            }
            Console.WriteLine("");
            Console.WriteLine("**************************************");
            Console.WriteLine("List of matched Customers");
            foreach (string customer in customers)
            {
                if(customer.Split('\t')[0].ToLower().Equals(fname.ToLower()) )
                    Console.WriteLine(customer);
            }

        }

    }

    class InputExecutor
    {
        static FileManipulation f = new FileManipulation();
        public static void insertCustomer(Customer customer)
        {
            string[] customerLines = { customer.getFname() + "\t" + customer.getLname() + "\t" + customer.getGender() + "\t" + customer.getAmountPaid() };
            f.insertToFile(customerLines);
        }
    }

}